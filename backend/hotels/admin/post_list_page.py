from ..models.post_list_page import Hotels, Component
from ..models.post_page import SingleHotel, Infrastructure
from django.contrib import admin
from garpix_page.admin import BasePageAdmin


@admin.register(Hotels)
class HotelsAdmin(BasePageAdmin):
    pass


@admin.register(SingleHotel)
class SingleHotelAdmin(BasePageAdmin):
    pass


class InfrastructureAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'content')


class ComponentAdmin(admin.ModelAdmin):
    list_display = ('id', 'template', 'title', 'is_active', 'sort')


admin.site.register(Infrastructure, InfrastructureAdmin)
admin.site.register(Component, ComponentAdmin)
