from django.db import models
from garpix_page.models import BasePage


class Infrastructure(models.Model):
    title = models.CharField(max_length=100, verbose_name='Инфраструктура')
    content = models.CharField(max_length=100, verbose_name='Содержание', blank=True, default='')

    class Meta:
        verbose_name = 'Инфраструктура'
        verbose_name_plural = 'Инфраструктура'

    def __str__(self):
        return self.title


CHOICES = (
    ('Гостиница', "Гостиница"),
    ("Мотель", "Мотель"),
    ("Апартаменты", "Апартаменты"),
)


class SingleHotel(BasePage):
    template = "pages/singleHotel.html"
    type = models.CharField(max_length=100, verbose_name='Тип жилья', choices=CHOICES)
    description = models.CharField(max_length=255, verbose_name='Описание', blank=True, default='')
    infrastructure = models.ManyToManyField(Infrastructure, blank=True, verbose_name='Инфраструктура')
    stars = models.PositiveIntegerField(verbose_name='Звезды', blank=True)
    rating = models.FloatField(verbose_name='Рейтинг', blank=True)
    price = models.PositiveIntegerField(verbose_name='Цена')
    image = models.ImageField(verbose_name='Фото', blank=True)

    class Meta:
        verbose_name = "Гостиница"
        verbose_name_plural = "Гостиницы"
        ordering = ("-created_at",)

    def get_infrastructure_title(self):
        return set([x.title for x in self.infrastructure.all()])

    def get_infrastructure_id(self):
        return set([x.id for x in self.infrastructure.all()])
