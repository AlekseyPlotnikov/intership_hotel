from django.db import models
from garpix_utils.paginator import GarpixPaginator
from django.db.models import Max
from garpix_page.models import BaseListPage
from .post_page import Infrastructure, SingleHotel, CHOICES


class Component(models.Model):
    class TEMPLATES:
        HOTELS = 'hotels.html'
        #MAIN_BANNER = 'main_banner.html'
        #REASON_RECOMMENDATION = 'Reason.html'
        #BOOKING = 'booking.html'
        HEAD = 'head.html'
        FOOTER = 'footer.html'

        TYPES = (
            (HOTELS, 'Гостиницы'),
            #(MAIN_BANNER, 'Главный баннер'),
            #(REASON_RECOMMENDATION, 'Причины'),
            #(BOOKING, 'Бронирование'),
            (HEAD, 'Шапка сайта'),
            (FOOTER, 'Подвал сайта'),
        )

    template = models.CharField(default='', max_length=100, choices=TEMPLATES.TYPES,
                                verbose_name='Шаблон')
    title = models.CharField(max_length=255, default='', verbose_name='Название шаблона')
    is_active = models.BooleanField(default=True, verbose_name='Включено')
    sort = models.IntegerField(default=100, verbose_name='Сортировка',
                               help_text='Чем меньше число, тем выше будет элемент в списке.')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Компонент"
        verbose_name_plural = "Компоненты"
        ordering = ('sort',)


class Hotels(BaseListPage):
    paginate_by = 3
    template = 'pages/hotels.html'
    components = models.ManyToManyField(Component, verbose_name='Компоненты страницы',
                                        blank=True)

    class Meta:
        verbose_name = "Список гостиниц"
        verbose_name_plural = "Списки гостиниц"
        ordering = ('-created_at',)

    def get_components(self):
        return self.components.filter(is_active=True)
'''
    def get_context(self, request=None, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        types = [x[1] for x in CHOICES]
        hotels = SingleHotel.objects.all()
        infrastructure = Infrastructure.objects.all()
        context.update({"infrastructure": infrastructure,
                        'types': types,
                        'obj_len': hotels,
                        'max_price': hotels.aggregate(Max('price')).get('price_max'),
                        })
        if request.GET:
            request_set = set(request.GET.dict())
            object_list = hotels
            try:
                min_price = int(request.GET.get('min-price'))
            except (ValueError, TypeError):
                min_price = 0
            try:
                max_price = int(request.GET.get('max-price'))
            except (ValueError, TypeError):
                max_price = 0
            infrastructure_set = set([x.title for x in infrastructure]).intersection(request_set)
            types_set = [x.upper() for x in set(types).intersection(request_set)]
            if types_set:
                object_list = hotels.filter(type__in=types_set)
            if min_price:
                object_list = object_list.filter(price__gte=min_price)
            if max_price:
                object_list = object_list.filter(price__lte=max_price)
                for i in infrastructure_set:
                    object_list = object_list.filter(infrastructure__title=i).distinct()
            ordering = request.GET.get('ordering')
            if ordering:
                if ordering[0] == '+':
                    object_list = object_list.order_by(ordering[1:])
                else:
                    object_list = object_list.order_by(ordering)

            paginator = GarpixPaginator(object_list, self.paginate_by)
            try:
                page = int(request.GET.get('page', 1))
            except ValueError:
                page = 1
            paginated_object_list = paginator.get_page(page)
            context.update({
                'paginator': paginator,
                'paginated_object_list': paginated_object_list,
                'page': page,
                'obj_len': len(object_list),
            })

        return context
'''