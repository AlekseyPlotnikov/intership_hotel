from modeltranslation.translator import TranslationOptions, register
from ..models.post_list_page import Hotels
from ..models.post_page import SingleHotel, Infrastructure


@register(Hotels)
class HotelsTranslationOptions(TranslationOptions):
    pass


@register(SingleHotel)
class SingleHotelTranslationOptions(TranslationOptions):
    pass


@register(Infrastructure)
class InfrastructureTranslationOptions(TranslationOptions):
    pass
