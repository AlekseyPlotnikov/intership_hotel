from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class SingleHotelConfig(AppConfig):
    name = 'singleHotel'
    verbose_name = _('Гостиницы')
