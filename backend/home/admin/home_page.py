from ..models.home_page import Recommendation, HomePage, Reason, \
    Component, Footer, Feedback, Booking
from django.contrib import admin
from ..models.blank import Blank
from garpix_page.admin import BasePageAdmin


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('id', 'date_in', 'date_out', 'adults', 'kids', 'created_at')


class BookingAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_content', 'second_content')


class RecommendationAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'number', 'content')


class FooterAdmin(admin.ModelAdmin):
    list_display = ('id', 'hashtag', 'phone_number')


class ReasonAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_title', 'main_title', 'title', 'number', 'content')


'''
class NameLinkAdmin(admin.ModelAdmin):
    list_display = ('id', 'content', 'sort')
'''


class ComponentAdmin(admin.ModelAdmin):
    list_display = ('id', 'template', 'title', 'is_active', 'sort')


@admin.register(HomePage)
class HomePageAdmin(admin.ModelAdmin):
    pass


@admin.register(Blank)
class BlankPageAdmin(BasePageAdmin):
    pass


admin.site.register(Reason, ReasonAdmin)
admin.site.register(Recommendation, RecommendationAdmin)
admin.site.register(Component, ComponentAdmin)
#admin.site.register(NameLink, NameLinkAdmin)
admin.site.register(Footer, FooterAdmin)
admin.site.register(Booking, BookingAdmin)
admin.site.register(Feedback, FeedbackAdmin)
