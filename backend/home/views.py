from django.shortcuts import render


def home_page(request):
    return render(request, 'pages/home.html', )


def hotels(request):
    return render(request, 'hotels.html', )


def blank(request):
    return render(request, 'blank.html')

