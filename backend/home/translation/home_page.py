from modeltranslation.translator import TranslationOptions, register
from ..models import HomePage, Reason, Recommendation
from ..models.blank import Blank


@register(HomePage)
class HomePageTranslationOptions(TranslationOptions):
    pass


@register(Reason)
class ReasonTranslationOptions(TranslationOptions):
    pass


@register(Recommendation)
class RecommendationTranslationOptions(TranslationOptions):
    pass


@register(Blank)
class BlankTranslationOptions(TranslationOptions):
    pass
