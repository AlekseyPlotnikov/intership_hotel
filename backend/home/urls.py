from django.urls import path
from . import views
from .views import home_page, blank, hotels

urlpatterns = [
    path('blank/', blank, name='blank'),
    path('hotels/', hotels, name='hotels'),
]

