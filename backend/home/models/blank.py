from garpix_page.models import BasePage


class Blank(BasePage):
    template = "blank.html"

    class Meta:
        verbose_name = "Blank"
        verbose_name_plural = "Blanks"
        ordering = ("-created_at",)