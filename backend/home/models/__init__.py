from .home_page import Recommendation, Reason, HomePage,\
    Component, Footer, Feedback, Booking, FeedbackForm  # noqa
from .blank import Blank
