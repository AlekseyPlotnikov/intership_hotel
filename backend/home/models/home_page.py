from django.db import models
from garpix_page.models import BasePage
from garpix_notify.models import Notify
from django.conf import settings
from django.forms import ModelForm


class Feedback(models.Model):
    date_in = models.CharField(max_length=100, verbose_name='Дата заезда')
    date_out = models.CharField(max_length=100, verbose_name='Дата выезда')
    adults = models.PositiveIntegerField(verbose_name='Количество взрослых')
    kids = models.PositiveIntegerField(verbose_name='Количество детей')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    class Meta:
        ordering = ('-created_at',)
        verbose_name = 'Обратная связь'
        verbose_name_plural = 'Обратная связь'


class FeedbackForm(ModelForm):
    class Meta:
        model = Feedback
        fields = '__all__'


'''class NameLink(models.Model):
    content = models.CharField(max_length=100, verbose_name='Название ссылки')
    sort = models.IntegerField(default=100, verbose_name='Сортировка',
                               help_text='Чем меньше число, тем выше будет элемент в списке.')

    class Meta:
        verbose_name = 'Название ссылки'
        verbose_name_plural = 'Названия ссылок'
        ordering = ('sort',)
'''


class Booking(models.Model):
    first_content = models.CharField(max_length=50, verbose_name='Первый текст')
    second_content = models.CharField(max_length=100, verbose_name='Второй текст')

    class Meta:
        verbose_name = 'Текст перед бронированием'
        verbose_name_plural = 'Текст перед бронированием'


class Footer(models.Model):
    hashtag = models.CharField(max_length=20, verbose_name='Хештэг')
    phone_number = models.CharField(max_length=20, verbose_name='Номер телефона')

    class Meta:
        verbose_name = 'Хештэг'
        verbose_name_plural = 'Хештэг'


class Recommendation(models.Model):
    title = models.CharField(max_length=100, verbose_name='Заголовок рекомендации')
    number = models.PositiveIntegerField()
    content = models.CharField(max_length=255, verbose_name='Содержание рекомендации')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Рекомендация'
        verbose_name_plural = 'Рекомендации'


class Reason(models.Model):
    first_title = models.CharField(max_length=200, default='', blank=True,
                                   verbose_name='Первый заголовок для всех причин')
    main_title = models.CharField(max_length=100, default='', blank=True,
                                  verbose_name='Основной заголовок для всех причин')
    title = models.CharField(max_length=100, verbose_name='Заголовок для каждой причины')
    number = models.PositiveIntegerField()
    content = models.CharField(max_length=255, verbose_name='Содержание причины')

    def __str__(self):
        return self.main_title

    class Meta:
        verbose_name = 'Причина'
        verbose_name_plural = 'Причины'


'''
def get_limit():
    return {'template__in': [

        'main_banner.html',
        'Reason.html',
        'booking.html',
        'head.html'
        'footer.html',
    ]}

'''


class Component(models.Model):
    class TEMPLATES:
        HOTELS = 'hotels'
        MAIN_BANNER = 'main_banner.html'
        REASON_RECOMMENDATION = 'Reason.html'
        BOOKING = 'booking.html'
        HEAD = 'head.html'
        FOOTER = 'footer.html'

        TYPES = (
            (HOTELS, 'Гостиницы'),
            (MAIN_BANNER, 'Главный баннер'),
            (REASON_RECOMMENDATION, 'Причины'),
            (BOOKING, 'Бронирование'),
            (HEAD, 'Шапка сайта'),
            (FOOTER, 'Подвал сайта'),
        )

    template = models.CharField(default='', max_length=100, choices=TEMPLATES.TYPES,
                                verbose_name='Шаблон')
    title = models.CharField(max_length=255, default='', verbose_name='Название шаблона')
    is_active = models.BooleanField(default=True, verbose_name='Включено')
    sort = models.IntegerField(default=100, verbose_name='Сортировка',
                               help_text='Чем меньше число, тем выше будет элемент в списке.')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Компонент"
        verbose_name_plural = "Компоненты"
        ordering = ('sort',)


class HomePage(BasePage):
    template = "pages/home.html"
    components = models.ManyToManyField(Component, verbose_name='Компоненты страницы',
                                        blank=True)  # , limit_choices_to=get_limit())

    class Meta:
        verbose_name = "Домашняя страница"
        verbose_name_plural = "Домашние страницы"
        ordering = ("-created_at",)

    def get_components(self):
        return self.components.filter(is_active=True)

    def get_context(self, request=None, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)

        booking = Booking.objects.all()
        #namelink = NameLink.objects.all().order_by('pk')[:4]
        #namelink_1 = namelink[0]
        #namelink_2 = namelink[1]
        #namelink_3 = namelink[2]
        #namelink_4 = namelink[3]
        footer = Footer.objects.all()
        recommendations = Recommendation.objects.all()
        reasons = Reason.objects.all().order_by('pk')[:5]
        reason_1 = reasons[0]
        reason_2 = reasons[1]
        reason_3_to_5 = reasons[2:5]

        context.update(
            {
                'booking': booking,
                #'namelink_1': namelink_1,
                #'namelink_2': namelink_2,
                #'namelink_3': namelink_3,
                #'namelink_4': namelink_4,
                'footer': footer,
                'recommendations': recommendations,
                'reason_1': reason_1,
                'reason_2': reason_2,
                'reason_3_to_5': reason_3_to_5,
            }
        )
        if request.method == 'POST':
            feedback_form = FeedbackForm(request.POST)
            if feedback_form.is_valid():
                feedback = feedback_form.save()
                Notify.send(settings.FEEDBACK_EVENT, {"feedback": feedback})
                context.update({
                    'message': 'Заявление о бронировании успешно отправлено',
                })

        return context

        '''
        if request.method == 'POST':
            from .feedback import FeedbackForm
            form = FeedbackForm(request.POST)
            if form.is_valid():
                feedback = form.save()
                Notify.send(settings.FEEDBACK_EVENT, {
                    'feedback': feedback,
                })
                context.update({
                    'message': 'Сообщение успешно отправлено',
                })
            context.update({
                'form': form,
            })
        return context
        '''
